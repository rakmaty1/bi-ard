# BI-ARD - semestrální práce

## Gameduino - hra Snake

Vytvořte na Gameduino (resp. MOD-VGA) hru Snake. Hra se bude ovládat tlačítky na Joystick shieldu, pomocí Joysticku bude možno hru pozastavit. Hra bude obsahovat více levelů. Ve hře bude umožněno ukládání a zobrazení nejvyšších skóre. Bude umožněno nastavení obtížnosti (rychlost pohybu hada).

## Uživatelská dokumentace

Po spuštění hry se hráč ocitne na domovské obrazovce.
Po obrazovce se pohybuje pomocí tlačítka nahoru a dolů, ke zvolení možnosti slouží tlačítko joysticku.

Na hlavní obrazovce si lze zobrazit dosažená nejvyšší skóre hráčů a zahájit novou hru.

Při zahájení nové hry si hráč může zvolit mód hry (klasický nebo s více levely - adventure).
Zvolení probíhá pomocí tlačítek vlevo/vpravo.
Také si lze zvolit obtížnost (lehká/střední/těžká), která určiúje rychost hada.

Poté lze hru zahájit, případně se vrátit na hlavní stránku.

Ve hře hráč ovládá hada pomocí tlačítek nahoru/dolů/vpravo/vlevo, přičemž se nelze otočit do opačného směru.
Hráč sbírá jídlo (odré body na obrazovce), za které získává body.
Přidělený počet bodů záleží na tom, jak dlouho hráči trvalo jídlo sebrat a také na zvolené obtížnosti.
Pokud hráč narazí sám do sebe, prohrává.

V módu adventure po snězení určitého počtu jídla bude následující fialové, které hráče přesune do dalšího levelu.
V těchto vyšších levelech se také nachází překážky (označené červeně), do kterých také hráč nesmí narazit.

Hru lze pozastavit stisknutím tlačítka joysticku.

Po konci hry bude hráč dotázán na jméno, které se uloží do tabulky nejvyšších skóre.