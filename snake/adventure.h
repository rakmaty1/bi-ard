int getAbsCoordX(byte x);
int getAbsCoordY(byte y);

//current maximum of body parts snake can have (excluding head)
byte maxParts;
//current remaining points that have to be collected to advance to next level in adventure mode
byte remain;

//following functions draw the walls in adventure mode

void adventure2()
{
  byte spr = 253;
  for (int i = 10; i < 40; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(13) + 4, 2, 0);
  for (int i = 10; i < 40; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(26) + 4, 3, 0);
  maxParts = spr + 1;
  remain = 15;
}

void adventure3()
{
  byte spr = 253;
  for (int i = 10; i < 40; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(9) + 4, 2, 0);
  GD.sprite(spr--, getAbsCoordX(39) + 4, getAbsCoordY(9) + 4, 2, 0);
  for (int i = 10; i < 40; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(21) + 4, 3, 0);
  GD.sprite(spr--, getAbsCoordX(39) + 4, getAbsCoordY(21) + 4, 3, 0);
  for (int i = 9; i < 15; i += 2)
    GD.sprite(spr--, getAbsCoordX(10) + 4, getAbsCoordY(i) + 4, 4, 0);
  for (int i = 17; i < 23; i += 2)
    GD.sprite(spr--, getAbsCoordX(10) + 4, getAbsCoordY(i) + 4, 4, 0);
  for (int i = 9; i < 15; i += 2)
    GD.sprite(spr--, getAbsCoordX(39) + 4, getAbsCoordY(i) + 4, 5, 0);
  for (int i = 17; i < 23; i += 2)
    GD.sprite(spr--, getAbsCoordX(39) + 4, getAbsCoordY(i) + 4, 5, 0);
  maxParts = spr + 1;
  remain = 15;
}

void adventure4()
{
  byte spr = 253;
  for (int i = 0; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(18) + 4, 2, 0);
  for (int i = 18; i < 37; i += 2)
    GD.sprite(spr--, getAbsCoordX(24) + 4, getAbsCoordY(i) + 4, 4, 0);
  maxParts = spr + 1;
  remain = 20;
}

void adventure5()
{
  byte spr = 253;
  for (int i = 0; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(18) + 4, 2, 0);
  for (int i = 4; i < 37; i += 2)
    GD.sprite(spr--, getAbsCoordX(24) + 4, getAbsCoordY(i) + 4, 4, 0);
  maxParts = spr + 1;
  remain = 20;
}

void adventure6()
{
  byte spr = 253;
  for (int i = 0; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(13) + 4, 2, 0);
  for (int i = 0; i < 42; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(26) + 4, 3, 0);
  for (int i = 4; i < 37; i += 2)
    GD.sprite(spr--, getAbsCoordX(24) + 4, getAbsCoordY(i) + 4, 4, 0);
  maxParts = spr + 1;
  remain = 20;
}

void adventure7()
{
  byte spr = 253;
  for (int i = 0; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(4) + 4, 2, 0);
  for (int i = 0; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(35) + 4, 3, 0);
  for (int i = 4; i < 36; i += 2)
    GD.sprite(spr--, getAbsCoordX(0) + 4, getAbsCoordY(i) + 4, 4, 0);
  GD.sprite(spr--, getAbsCoordX(0) + 4, getAbsCoordY(35), 4, 0);
  for (int i = 4; i < 36; i += 2)
    GD.sprite(spr--, getAbsCoordX(48) + 4, getAbsCoordY(i) + 4, 5, 0);
  GD.sprite(spr--, getAbsCoordX(48) + 4, getAbsCoordY(35), 5, 0);
  maxParts = spr + 1;
  remain = 25;
}

void adventure8()
{
  byte spr = 253;
  for (int i = 0; i < 24; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(4) + 4, 2, 0);
  for (int i = 28; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(4) + 4, 2, 0);
  for (int i = 0; i < 24; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(35) + 4, 3, 0);
  for (int i = 28; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(35) + 4, 3, 0);
  for (int i = 4; i < 16; i += 2)
    GD.sprite(spr--, getAbsCoordX(0) + 4, getAbsCoordY(i) + 4, 4, 0);
  for (int i = 18; i < 35; i += 2)
    GD.sprite(spr--, getAbsCoordX(0) + 4, getAbsCoordY(i) + 4, 4, 0);
  GD.sprite(spr--, getAbsCoordX(0) + 4, getAbsCoordY(35), 4, 0);
  for (int i = 4; i < 16; i += 2)
    GD.sprite(spr--, getAbsCoordX(48) + 4, getAbsCoordY(i) + 4, 5, 0);
  for (int i = 18; i < 35; i += 2)
    GD.sprite(spr--, getAbsCoordX(48) + 4, getAbsCoordY(i) + 4, 5, 0);
  GD.sprite(spr--, getAbsCoordX(48) + 4, getAbsCoordY(35), 5, 0);
  maxParts = spr + 1;
  remain = 25;
}

void adventure9()
{
  byte spr = 253;
  for (int i = 0; i < 10; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(4) + 4, 2, 0);
  for (int i = 40; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(4) + 4, 2, 0);
  for (int i = 0; i < 10; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(35) + 4, 3, 0);
  for (int i = 40; i < 50; i += 2)
    GD.sprite(spr--, getAbsCoordX(i) + 4, getAbsCoordY(35) + 4, 3, 0);
  for (int i = 4; i < 8; i += 2)
    GD.sprite(spr--, getAbsCoordX(0) + 4, getAbsCoordY(i) + 4, 4, 0);
  for (int i = 33; i < 37; i += 2)
    GD.sprite(spr--, getAbsCoordX(0) + 4, getAbsCoordY(i) + 4, 4, 0);
  for (int i = 4; i < 8; i += 2)
    GD.sprite(spr--, getAbsCoordX(48) + 4, getAbsCoordY(i) + 4, 5, 0);
  for (int i = 33; i < 37; i += 2)
    GD.sprite(spr--, getAbsCoordX(48) + 4, getAbsCoordY(i) + 4, 5, 0);
  maxParts = spr + 1;
}
