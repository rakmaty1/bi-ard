#include <SPI.h>
#include <GD.h>
#include <EEPROM.h>
#include "sprites.h"
#include "background.h"
#include "cp437.h"
#include "adventure.h"

/**
 * By changing this value before compilation, the game will run either on 60 or 72 fps.
 * Note that difficulity will be easier on 60 fps, because snake will move on less frames than on 72 fps.
 * Options are MODE_800x600_60 or MODE_800x600_72
 */
#define FPS MODE_800x600_60

#define UP 2 // UP button
#define DOWN 4 // DOWN button
#define LEFT 5 // LEFT button
#define RIGHT 3 // RIGHT button
#define JOYSTICK 8 // JOYSTICK button
#define DELAY 30 // determines, how many ms has to pass after last change on button for new change to be registred
#define MAX_PARTS 254 // maximum possible number of snake body parts, actuall amount may be lower
#define SPR_HEAD 255 // number of sprite of snakes head
#define SPR_FOOD 254 // number of sprite of food

//constants for states of the game
#define ST_MENU_P 0
#define ST_MENU_H 1
#define ST_HISCORE 2
#define ST_PLAY_MODE 3
#define ST_PLAY_DIFF 4
#define ST_PLAY_PLAY 5
#define ST_PLAY_BACK 6
#define ST_PAUSE_RES 7
#define ST_PAUSE_QUIT 8
#define ST_INGAME 9
#define ST_GAME_OVER 10


//constants for modes
#define M_CLASSIC 0
#define M_ADVENTURE 1

//constants for difficulities
#define D_EASY 8
#define D_MEDIUM 6
#define D_HARD 4

//constants for background
#define BG_MENU 0
#define BG_HIGHSCORE 1
#define BG_PLAY 2
#define BG_INGAME 3
#define BG_PAUSE 4
#define BG_GAME_OVER 5

/**
 * Struct which represents a coordinate.
 */
typedef struct coord
{
  byte x;
  byte y;
} Coord;

// current state of the game
byte state = ST_MENU_P;
// next state of the game
byte next_state = ST_MENU_P;
// current mode of the game
byte mode = M_CLASSIC;
// current difficulity of the game
byte diff = D_EASY;
// current backgroud of the game, default 255 ensures that background will change on startup
byte background = 255;
//stores how many frames has elapsed since last updating of snakes position
byte frame;
// current score that player will earn when eating next food
byte foodScore;
// current players score
unsigned long score;
// current level of the adventure mode
byte level;
// after checking snakes collision, the result is svaed in this variable, 0xff means no collision
byte headCollision = 0xff;
// relative coordinates of snakes head
Coord head;
// old coordinates of head, indicates where the head was before moving
Coord oldHead;
// current direction of snake
byte currentDirection;
// new direction of snake, prevents changing direction to opposite when clicking very fast
byte newDirection;
// current number of body parts of snake excluding head
byte bodyParts;
// index of sprite, which shows last part of snake
byte tail;
// stores position of snakes parts (excluding head), needed for generating food
Coord parts[MAX_PARTS];
// indiates if the score player got should be in highscores
bool newHiscore;
// current index of the letter in players name which is changing
byte newHiscoreIndex;
// name of player that got new highscore 
char newHiscoreName[11];

/**
 * This function will write a text on the screen using cp437 font.
 * This font uses two characters.
 * If the background isn't scrolled, the x coordinates 0 - 49 and y coordinates 0 - 17 are visible fully and 18 partially
 * @param x - x coordinate
 * @param y - y coordinate
 * @param s - the string to be written
 * @param halfLine - determines, if the the text should be one character line lower than usual.
 */
static void putstr16x8(int x, int y, const char *s, bool halfLine = false)
{
  uint16_t addr = (y << 7) + x;
  while (*s)
  {
    uint16_t w = pgm_read_word(cp437_pic + 2 * *s);
    GD.wr(addr + 64 * halfLine, lowByte(w));
    GD.wr(addr + 64 + 64 * halfLine, highByte(w));
    s++, addr++;
  }
}

/**
 * Arduino setup function, runs on boot.
 */
void setup()
{
  // Have some time for the gameduino to load (~250ms needed), more time allows the cool splashscreen to show up
  delay(5000);
  GD.begin();

  // Set fps
  GD.wr(VIDEO_MODE, FPS);
  
  // Load the cp437 font
  GD.uncompress(RAM_CHR, cp437_chr);
  GD.uncompress(RAM_PAL, cp437_pal);

  // Load background graphics
  GD.copy(RAM_PAL, pallete, sizeof(pallete));
  GD.copy(RAM_CHR, rect, sizeof(rect));
  GD.copy(RAM_CHR + 32, line, sizeof(line));

  //make characters - and > red (81 - ,110 111 >)
  GD.copy(RAM_PAL + 8 * 81, red, sizeof(red));
  GD.copy(RAM_PAL + 8 * 110, red, sizeof(red));
  GD.copy(RAM_PAL + 8 * 111, red, sizeof(red));

  // Load sprite graphics
  GD.copy(RAM_SPRIMG, sprites, sizeof(sprites));
  loadPallete();

  // Disable showing of sprites
  GD.wr(SPR_DISABLE, 1);

  // Seed the random generator with something hopefully random
  randomSeed(analogRead(0));

  // Setup buttons
  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(LEFT, INPUT_PULLUP);
  pinMode(RIGHT, INPUT_PULLUP);
  pinMode(JOYSTICK, INPUT_PULLUP);

  // Load default highscores if there arent valid data in EEPROM
  if (!highScoreValid())
    defaultHighScores();
}

// used in the loop function, determines if last loop was a vblank or not
bool lastVB = false;

/**
 * Aruino loop function.
 * It checks for button presses, and changes state appropriately.
 * If there is a vblank, it calls functions to change the screen contents.
 */
void loop()
{
  //set new state depending on button or set new snake direction
  byte button = checkButton();
  switch (state)
  {
    case ST_MENU_P:
      if (button == DOWN)
        next_state = ST_MENU_H;
      else if (button == JOYSTICK)
        next_state = ST_PLAY_MODE;
      break;
    case ST_MENU_H:
      if (button == UP)
        next_state = ST_MENU_P;
      else if (button == JOYSTICK)
        next_state = ST_HISCORE;
      break;
    case ST_HISCORE:
      if (button == JOYSTICK)
        next_state = ST_MENU_H;
      break;
    case ST_PLAY_MODE:
      if (button == DOWN)
        next_state = ST_PLAY_DIFF;
      else if (button == RIGHT)
        mode = M_ADVENTURE;
      else if (button == LEFT)
        mode = M_CLASSIC;
      break;
    case ST_PLAY_DIFF:
      if (button == UP)
        next_state = ST_PLAY_MODE;
      else if (button == DOWN)
        next_state = ST_PLAY_PLAY;
      else if (button == RIGHT)
      {
        if (diff == D_EASY)
          diff = D_MEDIUM;
        else if (diff == D_MEDIUM)
          diff = D_HARD;
      }
      else if (button == LEFT)
      {
        if (diff == D_HARD)
          diff = D_MEDIUM;
        else if (diff == D_MEDIUM)
          diff = D_EASY;
      }
      break;
    case ST_PLAY_PLAY:
      if (button == UP)
        next_state = ST_PLAY_DIFF;
      else if (button == DOWN)
        next_state = ST_PLAY_BACK;
      else if (button == JOYSTICK)
      {
        initializeGame();
        next_state = ST_INGAME;
      }
      break;
    case ST_PLAY_BACK:
      if (button == UP)
        next_state = ST_PLAY_PLAY;
      else if (button == JOYSTICK)
        next_state = ST_MENU_P;
      break;
    case ST_PAUSE_RES:
      if (button == DOWN)
        next_state = ST_PAUSE_QUIT;
      else if (button == JOYSTICK)
        next_state = ST_INGAME;
      break;
    case ST_PAUSE_QUIT:
      if (button == UP)
        next_state = ST_PAUSE_RES;
      else if (button == JOYSTICK)
        next_state = ST_MENU_P;
      break;
    case ST_INGAME:
      if (button == JOYSTICK)
        next_state = ST_PAUSE_RES;
      else
        setNewDirection(button);
      break;
    case ST_GAME_OVER:
      if (button == RIGHT && newHiscoreIndex != 9)
        newHiscoreIndex++;
      else if (button == LEFT && newHiscoreIndex != 0)
        newHiscoreIndex--;
      else if (button == UP)
        charInc();
      else if (button == DOWN)
        charDec();
      else if (button == JOYSTICK)
      {
        if (newHiscore)
          newHighscore(newHiscoreName, score);
        next_state = ST_MENU_P;
      }
      break;
  }
  state = next_state;
  
  //check for vblank, if 0, repeat, if 1, check collisions and update graphics
  if (GD.rd(VBLANK) == 1)
  {
    if (lastVB == false)
    {
      //fetch collisions and update screen
      collisionCheck();
      redraw();
      if (GD.rd(VBLANK) == 1)
        lastVB = true;
    }
  }
  else
    lastVB = false;
}

/**
 * Redraws the screen depending on current state.
 */
void redraw()
{
  switch (state)
  {
    case ST_MENU_P:
    case ST_MENU_H:
      drawMenu();
      break;
    case ST_HISCORE:
      drawHiscore();
      break;
    case ST_PLAY_MODE:
    case ST_PLAY_DIFF:
    case ST_PLAY_PLAY:
    case ST_PLAY_BACK:
      drawPlayScreen();
      break;
    case ST_INGAME:
      drawIngame();
      break;
    case ST_PAUSE_RES:
    case ST_PAUSE_QUIT:
      drawPause();
      break;
    case ST_GAME_OVER:
      drawGameOver();
      break;
  }
}

/**
 * Draws the menu screen, with currently selected option (play/hiscore) highlighted.
 */
void drawMenu()
{
  if (background != BG_MENU)
  {
    GD.wr(SPR_DISABLE, 1);
    GD.copy(RAM_PIC, menu, sizeof(menu));
    putstr16x8(15, 10, "PLAY");
    putstr16x8(15, 13, "HIGH SCORES");
    background = BG_MENU;
  }
  if (state == ST_MENU_P)
  {
    putstr16x8(9, 10, "--->");
    putstr16x8(9, 13, "    ");
  }
  else
  {
    putstr16x8(9, 10, "    ");
    putstr16x8(9, 13, "--->");
  }
}

/**
 * Draws the high score table. High scores are stored in EEPROM
 */
void drawHiscore()
{
  if (background != BG_HIGHSCORE)
  {
    //make background black, rect contains only 32 zeroes
    for (int i = 0; i < 40 * 2; i++)
      GD.copy(RAM_PIC + 32 * i, rect, sizeof(rect));
    
    putstr16x8(19, 2, "HIGH SCORES");
    
    for (int i = 0; i < 140; i += 14)
    {
      char player[11];
      player[10] = 0;
      unsigned long playerScore = 0;
      int j;
      
      //read and show player name
      for (j = i; j < 10 + i; j++)
        player[j - i] = EEPROM.read(j);
      putstr16x8(10, 5 + i / 14, player);

      //read score
      EEPROM.get(j, playerScore);
      
      //make string of the number and show it
      char scorestr[11];
      scorestr[10] = 0;
      for (int k = 9; k >= 0; k--)
      {
        if (playerScore == 0 && k != 9)
          scorestr[k] = ' ';
        else
          scorestr[k] = playerScore % 10 + '0';
        playerScore /= 10;
      }
      putstr16x8(30, 5 + i / 14, scorestr);
    }
    putstr16x8(1, 17, "Press JOYSTICK button to return to the main menu.");
    background = BG_HIGHSCORE;
  }
}

/**
 * Draws the "play screen" where player can choose difficulity, game mode and start the game
 */
void drawPlayScreen()
{
  if (background != BG_PLAY)
  {
    GD.copy(RAM_PIC, menu, sizeof(menu));
    putstr16x8(15, 8, "GAME MODE");
    putstr16x8(15, 11, "DIFFICULITY");
    putstr16x8(15, 14, "START GAME");
    putstr16x8(15, 17, "BACK");
    background = BG_PLAY;
  }
  char arrowl[] = {17, 0};
  char arrowr[] = {16, 0};
  
  if (mode == M_CLASSIC)
  {
    putstr16x8(27, 9, "   CLASSIC   ");
    putstr16x8(39, 9, arrowr);
  }
  else
  {
    putstr16x8(27, 9, "   ADVENTURE ");
    putstr16x8(27, 9, arrowl);
  }

  if (diff == D_EASY)
  {
    putstr16x8(27, 12, "   EASY     ");
    putstr16x8(36, 12, arrowr);
  }
  else if (diff == D_MEDIUM)
  {
    putstr16x8(27, 12, "   MEDIUM   ");
    putstr16x8(27, 12, arrowl);
    putstr16x8(38, 12, arrowr);
  }
  else
  {
    putstr16x8(27, 12, "   HARD     ");
    putstr16x8(27, 12, arrowl);
  }
  
  if (state == ST_PLAY_MODE)
  {
    putstr16x8(9,  8, "--->");
    putstr16x8(9, 11, "    ");
    putstr16x8(9, 14, "    ");
    putstr16x8(9, 17, "    ");
  }
  else if (state == ST_PLAY_DIFF)
  {
    putstr16x8(9,  8, "    ");
    putstr16x8(9, 11, "--->");
    putstr16x8(9, 14, "    ");
    putstr16x8(9, 17, "    ");
  }
  else if (state == ST_PLAY_PLAY)
  {
    putstr16x8(9,  8, "    ");
    putstr16x8(9, 11, "    ");
    putstr16x8(9, 14, "--->");
    putstr16x8(9, 17, "    ");
  }
  else
  {
    putstr16x8(9,  8, "    ");
    putstr16x8(9, 11, "    ");
    putstr16x8(9, 14, "    ");
    putstr16x8(9, 17, "--->");
  }
}

/**
 * Initializes a new game.
 * Since the classic and adventures first level are almost the same, there is no much difference.
 */
void initializeGame()
{
  maxParts = MAX_PARTS;
  bodyParts = 4;
  head.x = 20;
  head.y = 10;
  oldHead.x = 20;
  oldHead.y = 10;
  currentDirection = RIGHT;
  newDirection = RIGHT;
  tail = 0;
  score = 0;
  frame = 0;

  GD.sprite(SPR_HEAD, getAbsCoordX(head.x), getAbsCoordY(head.y), 0, 0);
  for (int i = 4; i < SPR_FOOD; i++)
    GD.sprite(i, 500, 500, 0, 0);
  GD.sprite(3, getAbsCoordX(19), getAbsCoordY(10), 0, 0);
  GD.sprite(2, getAbsCoordX(18), getAbsCoordY(10), 0, 0);
  GD.sprite(1, getAbsCoordX(17), getAbsCoordY(10), 0, 0);
  GD.sprite(0, getAbsCoordX(16), getAbsCoordY(10), 0, 0);
  GD.sprite(SPR_FOOD, getAbsCoordX(30), getAbsCoordY(17), 1, 0);
  foodScore = 255;
  memset(parts, 0, MAX_PARTS * sizeof(Coord));
  
  if (mode == M_ADVENTURE)
  {
    level = 1;
    remain = 10;
  }
}


/**
 * Draw the ingame screen and the snake.
 * If the screen isn't on ingame, shows Get ready! for 2 seconds.
 * Than shows snake and counts from 3 (each half second) to start the game.
 * In this time, the game will ignore all inputs.
 */
void drawIngame()
{
  if (background != BG_INGAME)
  {
    GD.copy(RAM_PIC, ingame, sizeof(ingame));
    if (mode == M_CLASSIC)
      putstr16x8(2, 0, "CLASSIC", true);
    else
    {
      putstr16x8(2, 0, "LEVEL: ", true);
      char l[] = {level + '0', 0};
      putstr16x8(9, 0, l, true);
    }
    
    putstr16x8(31, 0, "SCORE: ", true);
    unsigned long tmpScore = score;
    char scorestr[11];
    scorestr[10] = 0;
    for (int k = 9; k >= 0; k--)
    {
      if (tmpScore == 0 && k != 9)
        scorestr[k] = ' ';
      else
        scorestr[k] = tmpScore % 10 + '0';
      tmpScore /= 10;
    }
    putstr16x8(38, 0, scorestr, true);
    

    //Count from 3 to 0 and start
    putstr16x8(20, 5, "GET READY!");
    byte fps = FPS ? 72 : 60;
    for (int i = 0; i < 2 * fps; i++)
      GD.waitvblank();
    putstr16x8(20, 5, "          ");
    GD.wr(SPR_DISABLE, 0);
    
    putstr16x8(24, 0, "3", true);
    for (int i = 0; i < fps / 2; i++)
      GD.waitvblank();
    putstr16x8(24, 0, "2", true);
    for (int i = 0; i < fps / 2; i++)
      GD.waitvblank();
    putstr16x8(24, 0, "1", true);
    for (int i = 0; i < fps / 2; i++)
      GD.waitvblank();
    putstr16x8(24, 0, " ", true);
    background = BG_INGAME;
  }
  
  if (frame == 0 && headCollision != 0xff)
  {
    if (headCollision == SPR_FOOD)
      foodEaten();
    else
      gameOver();
  }
  
  if (frame == diff - 1)
  {
    currentDirection = newDirection;
    moveSnake();
    
    GD.sprite(tail, getAbsCoordX(oldHead.x), getAbsCoordY(oldHead.y), 0, 0);
    GD.sprite(SPR_HEAD, getAbsCoordX(head.x), getAbsCoordY(head.y), 0, 0);
    parts[tail].x = oldHead.x;
    parts[tail++].y = oldHead.y;
    oldHead.x = head.x;
    oldHead.y = head.y;
    if (tail == bodyParts)
      tail = 0;
    frame = 0;
    
    if (foodScore > 10)
      foodScore--;
  }
  else
    frame++;
}

/**
 * Adds score and generates new food after the last one was eaten.
 * If this is adventure mode and this is the last food in level, it sets its color to purple.
 */
void foodEaten()
{
  if (mode == M_ADVENTURE)
  {
    if (remain == 0)
      newLevel();
    else if (level != 9 && --remain == 0)
      GD.wr16(RAM_SPRPAL + 2, RGB(160, 0, 160));
  }
  
  byte mult = diff == D_EASY ? 1 : (diff == D_MEDIUM ? 2 : 3);
  score += foodScore * mult;

  if (bodyParts != maxParts)
  {
    GD.sprite(bodyParts, getAbsCoordX(head.x), getAbsCoordY(head.y), 0, 0);
    bodyParts++;
  }
  newFood();

  unsigned long tmpScore = score;
  char scorestr[11];
  scorestr[10] = 0;
  for (int k = 9; k >= 0; k--)
  {
    if (tmpScore == 0 && k != 9)
      scorestr[k] = ' ';
    else
      scorestr[k] = tmpScore % 10 + '0';
    tmpScore /= 10;
  }
  putstr16x8(38, 0, scorestr, true);
}

/**
 * Blinks the scren when game is over.
 */
void gameOver()
{
  byte fps = FPS ? 72 : 60;
  for (int i = 0; i < 4; i++)
  {
    GD.wr(SPR_DISABLE, 1);
    for (int i = 0; i < fps / 4; i++)
      GD.waitvblank();
    GD.wr(SPR_DISABLE, 0);
    for (int i = 0; i < fps / 4; i++)
      GD.waitvblank();
  }
  GD.wr(SPR_DISABLE, 1);
  next_state = ST_GAME_OVER;
  state = ST_GAME_OVER;
}

/**
 * Loads new level in the adventure mode.
 */
void newLevel()
{
  GD.wr(SPR_DISABLE, 1);
  GD.wr16(RAM_SPRPAL + 2, RGB(0, 0, 255));
  background = BG_PAUSE;
  if (level != 9)
    level++;
  byte tmpLevel = level;
  unsigned long tmpScore = score;
  initializeGame();
  level = tmpLevel;
  score = tmpScore;
  
  if (level == 2)
    adventure2();
  else if (level == 3)
    adventure3();
  else if (level == 4)
    adventure4();
  else if (level == 5)
    adventure5();
  else if (level == 6)
    adventure6();
  else if (level == 7)
    adventure7();
  else if (level == 8)
    adventure8();
  else if (level == 9)
    adventure9();
}

/**
 * Draws the pause screen.
 */
void drawPause()
{
  if (background != BG_PAUSE)
  {
    GD.wr(SPR_DISABLE, 1);
    putstr16x8(18, 5, "GAME IS PAUSED");
    putstr16x8(15, 10, "RESUME");
    putstr16x8(15, 13, "QUIT");//todo maybe add your score wont be saved
    background = BG_PAUSE;
  }
  
  if (state == ST_PAUSE_RES)
  {
    putstr16x8(9, 10, "--->");
    putstr16x8(9, 13, "    ");
  }
  else
  {
    putstr16x8(9, 10, "    ");
    putstr16x8(9, 13, "--->");
  }
}

/**
 * Draws the game over screen, prompts player to enter his name if he got highscore.
 */
void drawGameOver()
{
  if (background != BG_GAME_OVER)
  {
    newHiscore = shouldBeHighscore(score);
    memset(newHiscoreName, 0, 11);
    newHiscoreIndex = 0;
    
    putstr16x8(20, 5, "GAME OVER");
    if (newHiscore)
      putstr16x8(4, 15, "Press JOYSTICK button to confirm your name.");
    else
      putstr16x8(1, 15, "Press JOYSTICK button to return to the main menu.");
    background = BG_GAME_OVER;
  }
  if (newHiscore)
  {
    char tempName[10];
    for (int i = 0; i < 10; i++)
      tempName[i] = newHiscoreName[i] == '\0' ? '_' : newHiscoreName[i];
      
    putstr16x8(20, 10, tempName);
    putstr16x8(20, 9, "          ");
    putstr16x8(20, 11, "          ");
    char arrowu[] = {30, 0};
    char arrowd[] = {31, 0};
    putstr16x8(20 + newHiscoreIndex, 9, arrowu);
    putstr16x8(20 + newHiscoreIndex, 11, arrowd);
  }
  else
  {
    putstr16x8(20, 9, "          ");
    putstr16x8(20, 10, "          ");
    putstr16x8(20, 11, "          ");
  }
}

/**
 * Increments character on the game over screen where player chooses name.
 */
void charInc()
{
  if (newHiscoreName[newHiscoreIndex] == '\0')
    newHiscoreName[newHiscoreIndex] = 'A';
  else if (newHiscoreName[newHiscoreIndex] == 'Z')
    newHiscoreName[newHiscoreIndex] = '0';
  else if (newHiscoreName[newHiscoreIndex] == '9')
    newHiscoreName[newHiscoreIndex] = '\0';
  else
    newHiscoreName[newHiscoreIndex]++;
}

/**
 * Decrements character on the game over screen where player chooses name.
 */
void charDec()
{
  if (newHiscoreName[newHiscoreIndex] == '\0')
    newHiscoreName[newHiscoreIndex] = '9';
  else if (newHiscoreName[newHiscoreIndex] == '0')
    newHiscoreName[newHiscoreIndex] = 'Z';
  else if (newHiscoreName[newHiscoreIndex] == 'A')
    newHiscoreName[newHiscoreIndex] = '\0';
  else
    newHiscoreName[newHiscoreIndex]--;
}


/**
 * Moves snake in current direction.
 */
void moveSnake()
{
  switch(currentDirection)
  {
    case UP:
      if (head.y == 4)
        head.y = 36;
      else
        head.y--;
      break;
    case DOWN:
      if (head.y == 36)
        head.y = 4;
      else
        head.y++;
      break;
    case LEFT:
      if (head.x == 0)
        head.x = 49;
      else
        head.x--;
      break;
    case RIGHT:
      if (head.x == 49)
        head.x = 0;
      else
        head.x++;
      break;
  }
}

/**
 * Generates new food and shows it on screen.
 */
void newFood()
{
  Coord newCoord = generateCoord();
  GD.sprite(SPR_FOOD, getAbsCoordX(newCoord.x), getAbsCoordY(newCoord.y), 1, 0);
  foodScore = 255;
}

/**
 * Generates new random coordinates (eg. for food)
 * these coordinates will not collide with the snake
 */
Coord generateCoord()
{
  while (true)
  {
    Coord c1;
    c1.x = random(0, 49);
    c1.y = random(4, 36);
    bool valid = c1.x != head.x || c1.y != head.y; 
    
    for (int i = 0; i < bodyParts; i++)
    {
      if (c1.x == parts[i].x && c1.y == parts[i].y)
        valid = false;
    }
  
    if (valid)
      return c1;
  } 
}

/**
 * Sets new direction of the snake.
 */
void setNewDirection(byte button)
{
  switch (button)
  {
    case UP:
      if (currentDirection != DOWN)
        newDirection = UP;
      break;
    case DOWN:
      if (currentDirection != UP)
        newDirection = DOWN;
      break;
    case LEFT:
      if (currentDirection != RIGHT)
        newDirection = LEFT;
      break;
    case RIGHT:
      if (currentDirection != LEFT)
        newDirection = RIGHT;
      break;
  }
}

/**
 * Checks for snakes head collision and stores it.
 * Also if the food collides woth something (walls in adventure), it generates a new one.
 */
void collisionCheck()
{
  headCollision = GD.rd(COLLISION + SPR_HEAD);
  if (GD.rd(COLLISION + SPR_FOOD) != 0xff)
    newFood();
}

/**
 * Return absolute coordinate X of sprite from a relative coordinate in 8x8 matrix
 */
int getAbsCoordX(byte coord)
{
  return coord * 8 - 4;
}

/**
 * Return absolute coordinate Y of sprite from a relative coordinate in 8x8 matrix
 */
int getAbsCoordY(byte coord)
{
  return coord * 8 - 4 + 2;
}

// indicates if button is pressed right now or not
bool pressUp = false;
bool pressDown = false;
bool pressLeft = false;
bool pressRight = false;
bool pressJoystick = false;
// miliseconds since last change on button
unsigned long lastUp = 0;
unsigned long lastDown = 0;
unsigned long lastLeft = 0;
unsigned long lastRight = 0;
unsigned long lastJoystick = 0;

/**
 * Checks for pressed button, returns value of pressed button, or 0 when nothing is pressed
 * Button are read only when DELAY ms has elapsed since last change on the button.
 */
byte checkButton()
{
  int up = digitalRead(UP);
  int down = digitalRead(DOWN);
  int left = digitalRead(LEFT);
  int right = digitalRead(RIGHT);
  int joystick = digitalRead(JOYSTICK);
  unsigned long now = millis();
  if (now - lastUp > DELAY)
  {
    if (up == LOW && !pressUp)
    {
      pressUp = true;
      lastUp = now;
      return UP;
    }
    else if (up == HIGH && pressUp)
    {
      pressUp = false;
      lastUp = now;
    }
  }
  if (now - lastDown > DELAY)
  {
    if (down == LOW && !pressDown)
    {
      pressDown = true;
      lastDown = now;
      return DOWN;
    }
    else if (down == HIGH && pressDown)
    {
      pressDown = false;
      lastDown = now;
    }
  }
  if (now - lastLeft > DELAY)
  {
    if (left == LOW && !pressLeft)
    {
      pressLeft = true;
      lastLeft = now;
      return LEFT;
    }
    else if (left == HIGH && pressLeft)
    {
      pressLeft = false;
      lastLeft = now;
    }
  }
  if (now - lastRight > DELAY)
  {
    if (right == LOW && !pressRight)
    {
      pressRight = true;
      lastRight = now;
      return RIGHT;
    }
    else if (right == HIGH && pressRight)
    {
      pressRight = false;
      lastRight = now;
    }
  }
  if (now - lastJoystick > DELAY)
  {
    if (joystick == LOW && !pressJoystick)
    {
      pressJoystick = true;
      lastJoystick = now;
      return JOYSTICK;
    }
    else if (joystick == HIGH && pressJoystick)
    {
      pressJoystick = false;
      lastJoystick = now;
    }
  }
  return 0;
}

/**
 * Check validity of stored highscores
 */
bool highScoreValid()
{
  int sum = 0;
  for (int i = 0; i < 141; i++)
    sum += EEPROM.read(i);
  if (sum % 37 == 1)
    return true;
  else return false;
}

/**
 * Sets default highscores to the EEPROM
 */
void defaultHighScores()
{
  char player[] = "PLAYER0\0\0\0\0\0\0\0";
  int sum = 0;
  for (int i = 0; i < 140; i += 14)
  {
    for (int j = 0; j < 14; j++)
    {
      EEPROM.update(i + j, player[j]);
      sum += player[j];
    }
    player[6] += i / 14 + 1;
  }
  int check = 37;
  check -= sum % 37;
  check++;
  EEPROM.update(140, (byte) check);
}

/**
 * Checks if given score should go in highscore table (is bigger than atleast the lowest one stored)
 * @param newScore - the new score that will be checked
 */
bool shouldBeHighscore(unsigned long newScore)
{
  unsigned long minScore = -1;
  for (int i = 10; i < 140; i += 14)
  {
    unsigned long score = 0;
    EEPROM.get(i, score);
    
    if (score < minScore)
      minScore = score;
  }
  return newScore > minScore;
}

/**
 * Sets new highscore. The lowest one stored is discarded and the new one is inserted at the right position.
 * @param player - the player name
 * @param score - the new score
 */
void newHighscore(const char* player, unsigned long score)
{
  //prepare the actuall array that will be written
  byte bytePlayer[10];
  memset(bytePlayer, 0, sizeof(bytePlayer));
  int len = strlen(player);
  if (len > 10)
    len = 10;
  for (int i = 0; i < len; i++)
    bytePlayer[i] = player[i];

  //load saved scores
  unsigned long scores[10];
  for (int i = 0; i < 10; i++)
    EEPROM.get(10 + i * 14, scores[i]);

  //compute where should we insert the new score
  int insert = 0;
  for (; insert < 10; insert++)
  {
    if (score > scores[insert])
      break;
  }

  if (insert == 10) return;

  //update EEPROM and store the new score
  for (int i = 125; i >= insert * 14; i--)
    EEPROM.update(i + 14, EEPROM.read(i));
  EEPROM.put(insert * 14, bytePlayer);
  EEPROM.put(insert * 14 + 10, score);

  //make highscores valid
  int sum = 0;
  for (int i = 0; i < 140; i++)
    sum += EEPROM.read(i);
  int check = 37;
  check -= sum % 37;
  check++;
  EEPROM.update(140, (byte) check);
}